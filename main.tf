provider "aws" {
    region = "ap-southeast-1"
}

resource "aws_instance" "ec2_instance" {
    ami = "${var.ami_id}"
    count = "${var.number_of_instances}"
    subnet_id = "${var.subnet_id}"
    instance_type = "${var.instance_type}"
    vpc_security_group_ids = ["sg-0443c83bd158be73d"]

} 

