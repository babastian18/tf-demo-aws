variable "instance_name" {
        description = "Name of the instance to be created"
        default = "pipeline-tf-instance"
}

variable "instance_type" {
        default = "t2.micro"
}

variable "subnet_id" {
        description = "The VPC subnet the instance(s) will be created in"
        default = "subnet-02f9da58416675e22"
}

variable "ami_id" {
        description = "The AMI to use"
        default = "ami-02453f5468b897e31"
}

variable "number_of_instances" {
        description = "number of instances to be created"
        default = 1
}


